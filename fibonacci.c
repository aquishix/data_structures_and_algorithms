#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <gmp.h>
#include <string.h>
#include <math.h>

typedef struct fib_trip fib_trip;

struct fib_trip {
    mpz_t a;
    mpz_t b;
    mpz_t c;
};

fib_trip pre_calc_ft[28];  // haha; 25 is arbitrary

void fib_trip_mul(fib_trip* store, fib_trip* op1, fib_trip* op2) {
    mpz_t aa, bb, cc, ab, bc;
    mpz_inits(aa, bb, cc, ab, bc, 0);
    
    mpz_mul(aa, op1->a, op2->a);
    mpz_mul(bb, op1->b, op2->b);
    mpz_mul(cc, op1->c, op2->c);
    mpz_mul(ab, op1->a, op2->b);
    mpz_mul(bc, op1->b, op2->c);
    
    mpz_add(store->a, aa, bb);
    mpz_add(store->b, ab, bc);
    mpz_add(store->c, bb, cc);
    
    mpz_clears(aa, bb, cc, ab, bc, 0);
}

void do_pre_calc() { // refactor this to use fib_trip_mul(-, -, -)
    mpz_t a2, b2, c2, ab2; 
    mpz_inits(a2, b2, c2, ab2, pre_calc_ft[0].a, pre_calc_ft[0].b, pre_calc_ft[0].c, 0);
    
    mpz_set_ui(pre_calc_ft[0].a, 0);
    mpz_set_ui(pre_calc_ft[0].b, 1);
    mpz_set_ui(pre_calc_ft[0].c, 1);

    for (int i = 1; i < 28; i++) {
        //printf("@ do_pre_calc calculation #%d\n", i);
        mpz_init(pre_calc_ft[i].a);
        mpz_init(pre_calc_ft[i].b);
        mpz_init(pre_calc_ft[i].c);
        
        mpz_mul(a2, pre_calc_ft[i - 1].a, pre_calc_ft[i - 1].a);
        mpz_mul(b2, pre_calc_ft[i - 1].b, pre_calc_ft[i - 1].b);
        mpz_mul(c2, pre_calc_ft[i - 1].c, pre_calc_ft[i - 1].c);
        mpz_mul(ab2, pre_calc_ft[i - 1].a, pre_calc_ft[i - 1].b);
        mpz_add(ab2, ab2, ab2);
        
        mpz_add(pre_calc_ft[i].a, a2, b2);
        mpz_add(pre_calc_ft[i].b, ab2, b2);
        mpz_add(pre_calc_ft[i].c, b2, c2);

    }

// Should clear the things, but I don't care.

}

unsigned long long fib_exp(unsigned long long n) {
    if (n < 0) return -1;
    if (n == 0 || n == 1) return 1;
    if (n > 1) return fib_exp(n - 1) + fib_exp(n - 2);
}

mpz_t* fib_lin(int n) {
    mpz_t* a = calloc(1, sizeof(mpz_t));
    mpz_t* b = calloc(1, sizeof(mpz_t)); 
    mpz_t* c = calloc(1, sizeof(mpz_t));
    mpz_inits(*a, *b, *c, 0);
    mpz_set_ui(*a, 0);
    mpz_set_ui(*b, 1);
    mpz_set_ui(*c, 1);
    
    //unsigned long long temp = 0;
    for (int i = 0; i < n; i++) {
        mpz_add(*c, *a, *b);
        mpz_set(*a, *b);
        mpz_set(*b, *c);
        //c = a + b;
        //a = b;
        //b = c;
    }
    return c;
}

mpz_t* fib_log(unsigned int n) {
    if (n > pow(2, 28)) {
        printf("Cannot calculate F_n for n > 2^28.  Exiting...\n");
        exit(1);
    }
    
    fib_trip* pre_return_me = calloc(1, sizeof(fib_trip)); // Write some damned initialization code!
    mpz_inits(pre_return_me->a, pre_return_me->b, pre_return_me->c, 0);
    mpz_set_ui(pre_return_me->a, 1);
    mpz_set_ui(pre_return_me->b, 0);
    mpz_set_ui(pre_return_me->c, 1);

    for (int m = 0; m < 24; m++) {
        //if ((n >> m) & 1) printf("%d ", m);
        //printf("a, b, c: %s %s %s \n", mpz_get_str(0, 10, pre_calc_ft[m].a), mpz_get_str(0, 10, pre_calc_ft[m].b), mpz_get_str(0, 10, pre_return_me[m].c));
        
        //printf("a, b, c: %s %s %s \n", mpz_get_str(0, 10, pre_return_me->a), mpz_get_str(0, 10, pre_return_me->b), mpz_get_str(0, 10, pre_return_me->c));
        
        if ((n >> m) & 1) fib_trip_mul(pre_return_me, pre_return_me, &pre_calc_ft[m]);
    }
    printf("\n"); 
    //mpz_clears(pre_return_me->a, pre_return_me->b, 0); // could uncomment this later
    return &pre_return_me->b;
}

void fib_log_fin_loop(int how_far) {
    printf("GOT HERE!\n");
    mpz_t* value = calloc(1, sizeof(mpz_t));
    mpz_init(*value);

    printf("--- LOGARITHMIC TIME COMPLEXITY FIBONACCI CALCULATION *BEGIN* ---\n");
    for (int i = 0; i < how_far; i++) {
        value = fib_log(i);
        printf("F_%d: %s\n", i, mpz_get_str(0, 10, *value));
    }
    printf("--- LOGARITHMIC TIME COMPLEXITY FIBONACCI CALCULATION *END* ---\n");
    
}

void fib_log_from_user_input() {
    printf("Enter the index of the desired Fibonacci #: ");
    char kb_in[100];
    scanf("%s", kb_in);
    int index = atoi((const char*)kb_in);
    printf("F_%d == %s\n", index, mpz_get_str(0, 10, *fib_log(index)));
}


void fib_exp_from_user_input() {
    printf("Enter the index of the desired Fibonacci #: ");
    char kb_in[100];
    scanf("%s", kb_in);
    int index = atoi((const char*)kb_in);
    printf("index == %d\n", index);
    printf("F_%d == %llu\n", index, fib_exp(index));
}

void fib_exp_inf_loop() {
    struct timespec epoch_i;
    struct timespec epoch_f;
    unsigned long long fib_result = -1;
    unsigned long long time_taken = -1;
    for (int i = 0; i < 100; i++) {
        timespec_get(&epoch_i, TIME_UTC);
        fib_result = fib_exp(i);
        timespec_get(&epoch_f, TIME_UTC);
        time_taken = (epoch_f.tv_nsec + epoch_f.tv_sec * 1000000000 - epoch_i.tv_nsec - epoch_i.tv_sec * 1000000000)/1000;
        printf("F_%llu == %llu, and it took %llu microseconds to compute.\n", (unsigned long long)i, fib_result, time_taken);
        
    }

}

void fib_lin_from_user_input() { // UNTESTED?
    printf("Enter the index of the desired Fibonacci #: ");
    char kb_in[100];
    scanf("%s", kb_in);
    int index = atoi((const char*)kb_in);
    printf("F_%d == %s\n", index, mpz_get_str(0, 10, *fib_lin(index)));
}

/*
void fib_lin_from_user_input() { // UNTESTED
    printf("Enter the index of the desired Fibonacci #: ");
    char kb_in[100];
    scanf("%s", kb_in);
    int index = atoi((const char*)kb_in);
    printf("index == %d\n", index);
    printf("F_%d == %llu\n", index, fib_lin((unsigned long long)index));
}
*/

/*
void fib_lin_inf_loop() { // fix to work with GMP
    unsigned long long old_value = 1;
    unsigned long long value = 1;
    printf("--- LINEAR TIME COMPLEXITY FIBONACCI CALCULATION *BEGIN* ---\n");
    for (int i = 0; true; i++) {
        value = fib_lin((unsigned long long)i);
        if (value < old_value) exit(1);
        printf("F_%d: %llu\n", i, value);
        old_value = value;
    }
}
*/

void fib_lin_fin_loop(int how_far) {
    printf("--- LINEAR TIME COMPLEXITY FIBONACCI CALCULATION *BEGIN* ---\n");
    for (int i = 0; i < how_far; i++) {
        printf("F_%d == %s\n", i, mpz_get_str(0, 10, *fib_lin(i)));
        //printf("F_%d: %llu\n", i, value);
    }
    printf("--- LINEAR TIME COMPLEXITY FIBONACCI CALCULATION *END* ---\n");

}

int main() {
    do_pre_calc();
    //fib_exp_from_user_input(); 
    //fib_lin_inf_loop();
    //fib_lin_fin_loop(10000);
    //fib_log_fin_loop(10000); // You'll be waiting a while.  =)
    //fib_lin_from_user_input();
    //fib_log_from_user_input();
    fib_exp_inf_loop();

    /*
    char* blah; 
    for (int i = 0; i < 6; i++) {
        blah = mpz_get_str(0, 10, pre_calc_ft[i].c);
        printf("F_%d: %s\n", (int)pow(2, i), blah);
    }    
    */

    //fib_log((int)pow(2, 24) - 1);


}

