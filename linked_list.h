typedef struct Node Node;
typedef struct Linked_List Linked_List;

struct Node {
    int key;
    int value;
    Node* next;
};

struct Linked_List {
    int n;
    Node* first;
};

Linked_List* create_linked_list(int);
void prepend_linked_list(Linked_List*, int);
void print_linked_list(Linked_List*);
int retrieve_from_linked_list(Linked_List*, int);
int find_in_linked_list(Linked_List*, int);
int find_key_in_linked_list(Linked_List*, int);
int has_key_in_linked_list(Linked_List*, int);
int initialize_linked_list(Linked_List*);
int populate_linked_list_from_array(Linked_List*, int*); // a little questionable because of the syntactic sugar of [] vs *


