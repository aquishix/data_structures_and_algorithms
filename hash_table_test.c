#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "linked_list.h"
#include "hash_table.h"

int main() {
    printf("Creating hashtable...\n");

    Hash_Table* ht = create_hash_table(100);
    /*
    insert_hash_table(ht, 1, 2);
    insert_hash_table(ht, 3, 7);
    insert_hash_table(ht, 17, 17);
    insert_hash_table(ht, 17, 17);
    */
    print_hash_table(ht);
  
    int j = 0; 
    int old_load = ht->load;
    for (int i = 0; i < 100000000; i++) {
        if (ht->load != old_load + i) printf("FAIL ON INSERT!\n");
        if (i * i < 0) j = -i * i; else j = i * i;
        insert_hash_table(ht, i, i + 1337);
    } 
    
    printf("ht->length == %d\n", ht->length);
    printf("ht->load == %d\n", ht->load);
   
    j = 0; 
    old_load = ht->load;
    for (int i = 0; i < 100000000; i++) {
        if (ht->load != old_load - i) printf("FAIL ON DELETE!\n");
        if (i * i < 0) j = -i * i; else j = i * i;
        delete_hash_table(ht, i);

    } 
    
    printf("ht->length == %d\n", ht->length);
    printf("ht->load == %d\n", ht->load);


    printf("Wee...\n");

}
