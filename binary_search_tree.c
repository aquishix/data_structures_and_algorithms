#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "binary_search_tree.h"

//void bst_delete(Binary_Search_Tree*, int);
//void bst_verify(Binary_Search_Tree*);
//void bst_rotate_left(Binary_Search_Tree*);
//void bst_rotate_right(Binary_Search_Tree*);

Binary_Search_Tree* bst_create(int root_value) {
    Binary_Search_Tree* to_return = malloc(sizeof(Binary_Search_Tree));
    Node* to_set = malloc(sizeof(Node));
    to_set->value = root_value;
    to_return->root = to_set;
    
    to_return->root->left_child = NULL;
    to_return->root->right_child = NULL;
    return to_return;
}

void bst_insert(Binary_Search_Tree* tree, int to_insert) {
    Node* node = tree->root;
    Node* new_node = malloc(sizeof(Node));
    new_node->value = to_insert;

    while (3+3==6) {
        if (to_insert < node->value && node->left_child == NULL) {
            node->left_child = new_node;
            return;
        }
        if (to_insert > node->value && node->right_child == NULL) {
            node->right_child = new_node;
            return;
        }
        if (to_insert < node->value && node->left_child != NULL) {
            node = node->left_child;
            continue;
        }
        if (to_insert > node->value && node->right_child != NULL) {
            node = node->right_child;
            continue;    
        }
        
    }
    
}

Node* bst_find(Binary_Search_Tree* tree, int to_find, bool return_parent) {
    if (tree == NULL) return NULL;
    Node* parent_node = NULL;
    Node* current_node = tree->root;
    while (current_node != NULL) {
        if (current_node->value == to_find) {
            if (return_parent) return parent_node else return current_node;
        } 
        if (current_node->left_child != NULL) {
            if (current_node->left_child->value >= to_find) {
                parent_node = current_node;
                current_node = current_node->left_child;
                continue;
            }
        }
        if (current_node->right_child != NULL) {
            if (current_node->right_child->value <= to_find) {
                parent_node = current_node;
                current_node = current_node->right_child;
                continue;
            }
        }
    return NULL;
    }   
}


void bst_delete(Binary_Search_Tree* tree, int to_delete) {
    if (tree == NULL) return;
    Node* local_root = bst_find(tree, to_delete);
    if (local_root == NULL) return;
    if (local_root->left_child == NULL && local_root->right_child == NULL) return;
    if (local_root->left_child != NULL) {
        if (local_root->left_child->right_child == NULL) {
            Node* new_root = local_root->left_child;
            new_root->right_child = local_root->right_child;
            free(local_root);
            local_root= new_root;
            return;
        }
        Node* current_node = tree->root->left_child;
        while (current_node->right_child->right_child != NULL) {
                current_node = current_node->right_child;
        }    
        
    }
}


void bst_print(Binary_Search_Tree* tree) {
    bst_print_node(tree->root);
    printf("\n");
}

// This is based on recursion and will only work for small trees...
// ...because C does not implement (sufficient?) recursion optimization.
void bst_print_node(Node* node) {
    if (node == NULL) return;
    bst_print_node(node->left_child);
    printf("%d ", node->value);
    bst_print_node(node->right_child);
}

int main() {
    printf("\n\n");
    Binary_Search_Tree* my_tree = bst_create(5);
     
    my_tree->root->left_child = malloc(sizeof(Node*));
    my_tree->root->right_child = malloc(sizeof(Node*));
    my_tree->root->left_child->value = 4;
    my_tree->root->right_child->value = 11;
    

    bst_print(my_tree);
    printf("Inserting values 3, 2, 12, 15, 100, 106, and 9...\n");
    bst_insert(my_tree, 3);
    bst_insert(my_tree, 2);
    bst_insert(my_tree, 12);
    bst_insert(my_tree, 15);
    bst_insert(my_tree, 100);
    bst_insert(my_tree, 106);
    bst_insert(my_tree, 9);

    bst_print(my_tree);
    printf("\n\n");

    Node* my_node = bst_find(my_tree, 0);
    bst_print_node(my_node);
    printf("\n\n");
}


