#include "linked_list.h"

typedef struct Hash_Table Hash_Table;

struct Hash_Table {
    int length;
    int load;
    Linked_List** slots;
};

Hash_Table* create_hash_table(int);
void delete_hash_table(Hash_Table*, int);
void insert_hash_table(Hash_Table*, int, int);
int get_value_hash_table(Hash_Table*, int);
void print_hash_table(Hash_Table*);

