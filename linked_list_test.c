#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"

int main() {
    printf("Creating Linked_List with 5 elements: 7, 13, 8, 0, 2...\n");

    Linked_List ll = *create_linked_list(5);

    populate_linked_list_from_array(&ll, (int[]){7, 13, 8, 0, 2});
    print_linked_list(&ll);
    
    printf("prepending 17...\n");
    prepend_linked_list(&ll, 17);
    print_linked_list(&ll);
    

    printf("Finding the value: 8\n");

    printf("Index for the value 8: %d\n", find_in_linked_list(&ll, 8));

    printf("Initializing list...\n");

    initialize_linked_list(&ll);

    print_linked_list(&ll);

    printf("\nValue at index 3: %d\n", retrieve_from_linked_list(&ll, 3));
    
    return 0;
}


