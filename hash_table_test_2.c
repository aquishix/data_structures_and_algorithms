#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "linked_list.h"
#include "hash_table.h"

int main() {
    printf("Creating hashtable...\n");

    Hash_Table* ht = create_hash_table(100);
    print_hash_table(ht);
  
    int j = 0; 
    int old_load = ht->load;
    int lengths[100];
    for (int i = 0; i < 100; i++) {
        if (ht->load != old_load + i) printf("FAIL ON INSERT!\n");
        lengths[i] = ht->length;
        insert_hash_table(ht, i + ht->length, i + 1337);
    } 
    
    printf("ht->length == %d\n", ht->length);
    printf("ht->load == %d\n", ht->load);
   
    j = 0; 
    old_load = ht->load;
    for (int i = 0; i < 100; i++) {
        if (ht->load != old_load - i) printf("FAIL ON DELETE!\n");
        delete_hash_table(ht, i + lengths[i]);

    } 
   

    printf("ht->length == %d\n", ht->length);
    printf("ht->load == %d\n", ht->load);

    insert_hash_table(ht, 12 * 1, 689);
    insert_hash_table(ht, 12 * 2, 689);
    insert_hash_table(ht, 12 * 3, 689);
    insert_hash_table(ht, 12 * 4, 689);
    insert_hash_table(ht, 12 * 5, 689);
    insert_hash_table(ht, 12 * 6, 689);

    printf("ht->slots[0]->n == %d\n", ht->slots[0]->n);
    delete_hash_table(ht, 12 * 4);
    printf("ht->slots[0]->n == %d\n", ht->slots[0]->n);
    
    printf("ht->slots[0]->n == %d\n", ht->slots[0]->n);
    delete_hash_table(ht, 12 * 1);
    printf("ht->slots[0]->n == %d\n", ht->slots[0]->n);
    
    printf("ht->length == %d\n", ht->length);
    printf("ht->load == %d\n", ht->load);

    printf("Wee...\n");

}
