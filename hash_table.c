#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "linked_list.h"
#include "hash_table.h"

Hash_Table* create_hash_table(int size) {
    Hash_Table* return_me = calloc(1, sizeof(Hash_Table));
    return_me->length = size;
    return_me->load = 0;
    return_me->slots = calloc(size, sizeof(Linked_List*));
    return return_me;
}

void delete_hash_table(Hash_Table* ht, int key) {
    int old_length = ht->length;
    /*
    printf("ht->length == %d\n", ht->length);
    printf("ht->load == %d\n", ht->load);
    */
    if (ht->load < (ht->length >> 3)) {
        Linked_List** old_slots = ht->slots;
        ht->length = ht->length >> 1;
        ht->load = 0; 
        ht->slots = calloc(ht->length, sizeof(Linked_List*));
        for (int i = 0; i < old_length; i++) {
            if (old_slots[i] != NULL) {
                Node* current_node = old_slots[i]->first;
                Node* next_node;
                    
                while (current_node != NULL) {
                    next_node = current_node->next;
                    insert_hash_table(ht, current_node->key, current_node->value);
                    free(current_node);
                    current_node = next_node;
                }
                free(old_slots[i]);
            }
        }
        free(old_slots);
    }
    
    int hash = key % ht->length;
    if (ht->slots[hash] == NULL) return;
    else {
        //if (ht->slots[hash]->n == 0) return; // this shouldn't do anything
        if (ht->slots[hash]->n == 1) {
            free(ht->slots[hash]->first); // unclaimable memory if something funky is going on
            free(ht->slots[hash]);
            ht->slots[hash] = NULL;
            ht->load--;
            return;
        }
        else { // Remember to refactor this out into its own function(s) in linked_list.c
            if (ht->slots[hash]->n > 2) printf("Interesting things happened.\n");
            Node* node = ht->slots[hash]->first;
            Node* previous_node = ht->slots[hash]->first; // misplaced paranoia
            for (int i = 0; i < ht->slots[hash]->n; i++) {
                if (node == NULL) printf("...and node == NULL.\n"); else printf("...and node != NULL.\n");
                if (node->key == key) {
                    if (i == 0) {
                        Node* delete_me = ht->slots[hash]->first;
                        ht->slots[hash]->first = ht->slots[hash]->first->next;
                        free(delete_me);
                        printf("...and before decrementing, ht->slots[hash]->n == %d\n", ht->slots[hash]->n);
                        ht->slots[hash]->n--; // error prone, if something funky is going on
                        if (ht->load > 0) ht->load--;
                        return;
                    }
                    else {
                        if (node->next == NULL) { // remember to fuck this up
                            previous_node->next = NULL;
                            free(node);
                            printf("...and before decrementing, ht->slots[hash]->n == %d\n", ht->slots[hash]->n);
                            ht->slots[hash]->n--; // error prone, if something funky is going on
                            if (ht->load > 0) ht->load--;
                            return;
                        }
                        else {
                            previous_node->next = node->next;
                            free(node);
                            printf("...and before decrementing, ht->slots[hash]->n == %d\n", ht->slots[hash]->n);
                            ht->slots[hash]->n--; // error prone, if something funky is going on
                            if (ht->load > 0) ht->load--;
                            return;
                        }
                    }
                }
                previous_node = node;
                printf("...and ht->slots[hash]->n == %d\n", ht->slots[hash]->n);
                node = node->next;
            }
        }
        if (ht->load > 0) ht->load--;
    }    
}

void insert_hash_table(Hash_Table* ht, int key, int value) {
    if (ht->load + 1 > (ht->length >> 1)) {
        Linked_List** old_slots = ht->slots;
        ht->length = ht->length << 1;
        ht->load = 0; 
        ht->slots = calloc(ht->length, sizeof(Linked_List*));
        for (int i = 0; i < ht->length >> 1; i++) {
            if (old_slots[i] != NULL) {
                Node* current_node = old_slots[i]->first;
                Node* next_node;
                    
                while (current_node != NULL) {
                    next_node = current_node->next;
                    insert_hash_table(ht, current_node->key, current_node->value);
                    free(current_node);
                    current_node = next_node;
                }
                free(old_slots[i]);
            }
        }
        free(old_slots);
    }
    
    int hash = key % ht->length;
    if (ht->slots[hash] == NULL) {
        printf("Creating linked list of size 1 with key and value: %d and %d\n", key, value);
        ht->slots[hash] = create_linked_list(1);
        ht->slots[hash]->first->key = key;
        ht->slots[hash]->first->value = value;
        ht->load++;
    }
    else {
        printf("iWtf?\n");
        if (has_key_in_linked_list(ht->slots[hash], key)) return;
        prepend_linked_list(ht->slots[hash], value); // leaky abstraction bullshit follows =)
        printf("ht->slots[hash]->n == %d\n", ht->slots[hash]->n); 
        ht->slots[hash]->first->key = key; // see previous comment
        
        ht->load++;
        
    }    
}

int get_value_hash_table(Hash_Table* ht, int key) {
    int hash = key % ht->length;
    if (ht->slots[hash] != NULL) {
        Node* current_node = ht->slots[hash]->first;
        for (int i = 0; i < ht->slots[hash]->n; i++) {
            if (current_node->key == key) return current_node->key;
            current_node = current_node->next;
        }
    }

    return -2147483648; // use #define for this instead
}

void print_hash_table(Hash_Table* ht) {
    for (int i = 0; i < ht->length; i++) {
        if (ht->slots[i] != NULL) {
            Node* current_node = ht->slots[i]->first;
            for (int j = 0; j < ht->slots[i]->n; j++) {
                printf("%d: %d\n", current_node->key, current_node->value);
                current_node = current_node->next;
            }
        }
    }
}

