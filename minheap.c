#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*
int verify_mindheapness(int* supposed_minheap, int length) {
    for (int i = 0;;) {} 
}
*/

typedef struct minheap minheap;

struct minheap {
    int* heap;
    int length;
    int vlength;
};

minheap new_minheap(int vlength) {
    int length = (int)pow(2, ceil(log2(vlength + 1)));  // not the best way to to accomplish this
    minheap return_me;
    return_me.heap = (int*)malloc(length * sizeof(int));
    return_me.length = length;
    return_me.vlength = vlength;
    return return_me;
}

minheap new_minheap_and_init(int vlength, int* array) { // can this be combined sensibly with put_on_heap?
    minheap return_me = new_minheap(vlength);
    for (int i = 0; i < vlength; i++) return_me.heap[i] = array[i];
    return return_me;
}

void print_minheap(minheap* heap) {
    int last_power_of_2 = 1;
    int tracker = 1;
    for (int i = 0; i < heap->vlength; i++) {
        printf("%d ", heap->heap[i]);
        if (tracker == last_power_of_2) {
            last_power_of_2 *= 2;
            tracker = 0;
            printf("\n");
        }
        tracker++; 
    }
    printf("\n");
}

int find(minheap* minheap, int to_find) {
    for (int i = 0; i < minheap->vlength; i++) {
        if (minheap->heap[i] == to_find) return i;
    }
    return -1;
}

void insert(minheap* heap, int to_insert) {
    if (heap->vlength == heap->length) {
        int length = (int)pow(2, ceil(log2(heap->vlength + 1)));
        int* new_heap = (int*)malloc(length * sizeof(int));
        for (int i = 0; i < heap->vlength; i++) new_heap[i] = heap->heap[i];
        free(heap->heap);
        heap->heap = new_heap;
        heap->length = length;
    }
    int index = heap->vlength;
    heap->heap[index] = to_insert;
    int temp = -1;
    int parent_index = (index - 1) >> 1;
    while (heap->heap[index] <= heap->heap[parent_index] && index > 0) {// I.e., if the child's value is less
            temp = heap->heap[index];
            heap->heap[index] = heap->heap[parent_index];
            heap->heap[parent_index] = temp;
            index = parent_index;
            parent_index = (index - 1) >> 1;
    }
    heap->vlength++;
}

void print_minheap_status(minheap* heap) {
    printf("----------------\n");
    printf("minheap length: %d\n", heap->length);
    printf("minheap vlength: %d\n", heap->vlength);
    printf("----------------\n");
}

void delete_min(minheap* heap) {
    if (heap->vlength <= heap->length >> 2 && heap->length >= 4) { // Could bump the '4' to '4096' or something even larger 
        int length = heap->length >> 1;
        int* new_heap = (int*)malloc(length * sizeof(int));
        for (int i = 0; i < heap->vlength; i++) new_heap[i] = heap->heap[i];
        free(heap->heap);
        heap->heap = new_heap;
        heap->length = length;
    }
    if (heap->vlength == 0) return;
    if (heap->vlength == 1) {
        heap->vlength--;
        return;
    }
    heap->heap[0] = heap->heap[heap->vlength - 1];
    heap->vlength--;
    int index = 0;
    int temp = -1;
    //if (heap->vlength == 1) return;
    while (2 * index + 1 < heap->vlength) { // left child exists
        if (2 * index + 2 >= heap->vlength) { // right child is OOB
            if (heap->heap[2 * index + 1] < heap->heap[index]) {
                temp = heap->heap[index];
                heap->heap[index] = heap->heap[2 * index + 1];
                heap->heap[2 * index + 1] = temp;
                index = 2 * index + 1;
            } else {break;}
        } else {
            if (heap->heap[2 * index + 1] < heap->heap[index] || heap->heap[2 * index + 2] < heap->heap[index]) { // a child's value is smaller than parent's
                if (heap->heap[2 * index + 1] < heap->heap[2 * index + 2]) { // lc < rc
                    temp = heap->heap[index];
                    heap->heap[index] = heap->heap[2 * index + 1];
                    heap->heap[2 * index + 1] = temp;
                    index = 2 * index + 1;
                } else {
                    temp = heap->heap[index];
                    heap->heap[index] = heap->heap[2 * index + 2];
                    heap->heap[2 * index + 2] = temp;
                    index = 2 * index + 2;                        
                }
            } else {break;}
            
        }
    }
}

int peek_min(minheap* heap) {
    if (heap->vlength == 0) return -1;
    return heap->heap[0];
}

int main() {
    //minheap my_minheap = new_minheap_and_init(9, (int[]){3, 5, 7, 10, 6, 9, 11, 13, 21}); // vim is stupid.
    minheap my_minheap = new_minheap(0);
    print_minheap(&my_minheap); 
   
    /*
    printf("Finding value: 10...\n");
    printf("Found at %d.\n", find(my_minheap, 10));
    */
  
    /* 
    print_minheap_status(&my_minheap); 
    printf("Inserting 2...\n");
    insert(&my_minheap, 2);
    print_minheap(&my_minheap);
    print_minheap_status(&my_minheap); 
    */

    for (int i = 10000000-1; i >= 0; i--) {
        //printf("Inserting %d...\n", i);
        if ((i+1) % 100000 == 0) printf("Inserting: %d\n", i);
        insert(&my_minheap, i);
        //print_minheap(&my_minheap);
    }

    for (int i = 0; i < 10000000; i++) {
        if ((i+1) % 1000000 == 0) printf("Before delete_min, peek_min yields: %d\n", peek_min(&my_minheap));
        //printf("Running delete_min...\n");
        delete_min(&my_minheap);
        if ((i+1) % 1000000 == 0) printf("After delete_min, peek_min yields: %d\n", peek_min(&my_minheap));
        //print_minheap(&my_minheap);
    }

    print_minheap_status(&my_minheap);
    print_minheap(&my_minheap); 
    return 0;
}


//CORRECT CODE!!!!!!!!!!!!!!!!!!!!!!!!
//AS IN, IT IS CORRECT.
//NOT AS IN, IT NEEDS TO BE CORRECTED.

