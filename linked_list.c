#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "linked_list.h"

void prepend_linked_list(Linked_List* ll, int value) {
    Node* new_head = malloc(sizeof(Node));
    new_head->next = ll->first;
    new_head->value = value;
    ll->first = new_head;
    //if (ll->n > -1) printf("Yayyyy.  ll->n == %d\n", ll->n);

    ll->n++;
}

Linked_List* create_linked_list(int n) {
    Node* previous_node = calloc(1, sizeof(Node));
    
    Linked_List* return_me = calloc(1, sizeof(Linked_List));
    return_me->n = n;
    return_me->first = previous_node;
    return_me->first->value = -1;

    Node* dummy_var;
    if (n > 1) {
        for (int i = 1; i < n; i++) {
            dummy_var = calloc(1, sizeof(Node));
            dummy_var->value = -1;
            previous_node->next = dummy_var;
            previous_node = dummy_var;
            dummy_var->next = NULL;
        }
    } 
    else {
        return_me->first->next = NULL;
    }
    return return_me;
}

void print_linked_list(Linked_List* ll) {
    printf("\nContents: \n");
    Node* node = ll->first;
    for (int i = 0; i < ll->n; i++) {
        printf("List value at node %d is: %d\n", i, node->value);
        if (node->next == NULL) break;
        node = node->next;
    }
}

int retrieve_from_linked_list(Linked_List* ll, int index) {
    Node* node = ll->first;
    for (int i = 0; i < ll->n; i++) {
        if (i == index) return node->value;
        if (node->next == NULL) break;
        node = node->next;
    }
    return -1;  // this line is bullshit
}

// should this be renamed to "find_value_in_linked_list(-,-)"?
int find_in_linked_list(Linked_List* ll, int find_me) {
    Node* node = ll->first;
    for (int i = 0; i < ll->n; i++) {
        if (node->value == find_me) return i;
        if (node->next == NULL) break;
        node = node->next;
    }
    return -1;  // this line is not bullshit
}

int find_key_in_linked_list(Linked_List* ll, int find_me) {
    Node* node = ll->first;
    for (int i = 0; i < ll->n; i++) {
        if (node->key == find_me) return i;
        if (node->next == NULL) break;
        node = node->next;
    }
    return -1;  // this line is not bullshit
}

int has_key_in_linked_list(Linked_List* ll, int find_me) {
    Node* node = ll->first;
    for (int i = 0; i < ll->n; i++) {
        if (node->key == find_me) return true;
        if (node->next == NULL) return false;
        node = node->next;
    }
    return false;  // this line WAS not bullshit
}

int initialize_linked_list(Linked_List* ll) {
    Node* node = ll->first;
    for (int i = 0; i < ll->n; i++) {
        node->value = 0;
        if (node->next == NULL) return 0;
        node = node->next;
    }
    return -1;  // this line is not bullshit 
}

// this function is fucking dangerous
int populate_linked_list_from_array(Linked_List* ll, int source[]) {
    Node* node = ll->first;
    for (int i = 0; i < ll->n; i++) {
        node->value = source[i];
        if (node->next == NULL) return 0;
        node = node->next;
    }
    return -1;  // this line is not bullshit
}



