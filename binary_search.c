#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int int_log2(int input) {
    if (input == 1) return 0;
    int raised;
    int output = 0;
    for (raised = 1; raised <= input; raised *= 2) {
        output++;
    }
    return output - 1;
}

void print_list(int* list, int n) {
    for (int i = 0; i < n; i++) {
        printf("value at index %d is: %d\n", i, list[i]);
    }
}

int find(int* list, int list_length, int to_find) {
    int index = (int)((double)list_length / 2.0);
    //printf("initial index is: %d\n", index); 
    //int jump_size = (int)(log(index)/log(2)) * 2;
    int jump_size = int_log2(index) * 2;
    while (3+3==6) {
        if (index < 0) {index = 0;}
        if (index >= list_length) {index = list_length - 1;}
        if (list[index] == to_find) return index;
        //printf ("index == %d\n", index);
        //printf ("jump_size == %d\n", jump_size);
        if (jump_size == 0) return -1;
        if (to_find < list[index]) {
            if (jump_size == 0) return -1;
            index -= jump_size;
            jump_size /= 2;    
        }
        if (list[index] < to_find) {
            if (jump_size == 0) return -1;
            index += jump_size;
            jump_size /= 2;
        }
        
    } 
    
    
    return -1; // C is an outsourced dumb secretary; hence the necessity for this line.
}

void display_attempt(int* list, int list_length, int to_find) {
    int result = find(list, 10, to_find);
    if (result == -1) {printf("Couldn't find %d in the list.\n", to_find);}
        else {printf("Found the value %d in the list at index %d.\n", to_find, result);}
}

int main() {
    int* list;
    list = (int*)malloc(10 * (int)sizeof(int));
    
    list[0] = 3;
    list[1] = 5;
    list[2] = 6;
    list[3] = 9;
    list[4] = 15;
    list[5] = 18;
    list[6] = 19;
    list[7] = 19;
    list[8] = 26;
    list[9] = 32;

    int to_find = 6;    

    print_list(list, 10);

    printf("\n");

    
    int targets[8] = {3, 5, 6, 9, 10, 25, 26, 32};
    for (int i = 0; i < 8; i++) {
        display_attempt(list, 10, targets[i]);
    }



    return 0;
}

