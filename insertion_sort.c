#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_list(int* list, int n) {
    for (int i = 0; i < n; i++) {
        printf("value at index %d is: %d\n", i, list[i]);
    }
}

void swap_values(int* list, int i, int j) {
    int temp = list[i];
    list[i] = list[j];
    list[j] = temp;
}

void insertion_sort(int* list, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = i; j > 0; j--) {
            if (list[j] < list[j-1]) swap_values(list, j, j-1);
                else break;
       }
   } 
}

int main() {
    int* list;
    list = (int*)malloc(8 * (int)sizeof(int));
    
    list[0] = 8;
    list[1] = 5;
    list[2] = 11;
    list[3] = 1;
    list[4] = 15;
    list[5] = 14;
    list[6] = 17;
    list[7] = 5;

    

    print_list(list, 8);

    printf("\n");

    insertion_sort(list, 8);
    
    print_list(list, 8);

    return 0;
}

