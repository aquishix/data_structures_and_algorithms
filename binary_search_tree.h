#include <stdbool.h>

typedef struct Node Node;
typedef struct Binary_Search_Tree Binary_Search_Tree;

struct Node {
    int value;
    Node* left_child;
    Node* right_child;
};

struct Binary_Search_Tree {
    Node* root;
};

Binary_Search_Tree* bst_create(int);
void bst_insert(Binary_Search_Tree*, int);
void bst_delete(Binary_Search_Tree*, int);
void bst_verify(Binary_Search_Tree*);
void bst_rotate_left(Binary_Search_Tree*);
void bst_rotate_right(Binary_Search_Tree*);
void bst_print(Binary_Search_Tree*);
void bst_print_node(Node*);

